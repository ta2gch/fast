﻿// Learn more about F# at http://fsharp.org
module Main

open Printf

[<EntryPoint>]
let main argv =
    Parser.parse "(F love (S love))"
    |> (fun ap -> Expression.toString ap |> printfn "%s"; ap)
    |> Compiler.transform
    |> (fun ap -> Expression.toString ap |> printfn "%s"; ap)
    |> Compiler.compile
    |> (fun op -> Operation.toString op |> printfn "%s"; op)
    |> Interpreter.execute Interpreter.Lazy
    |> (fun ap -> ap.Expression)
    |> (fun ap -> Expression.toString ap |> printfn "%s"; ap)
    |> ignore
    0 // return an integer exit code
