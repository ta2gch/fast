﻿module Function

open Expression
open Deferred

type Function = 
    { Apply: Deferred<Function> -> Deferred<Function>
      Expression: Expression }
let apply b = fun (a: Function) -> a.Apply b
let expression a = a.Expression