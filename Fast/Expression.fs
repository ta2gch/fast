﻿module Expression

type Expression =
| SymbolExp of string
| ListExp of Expression list

let fexp x y = ListExp [SymbolExp "F"; SymbolExp x; y]
let aexp x y = ListExp [SymbolExp "A"; x; y]
let sexp x = ListExp [SymbolExp "S"; SymbolExp x]

let rec toString exp =
    match exp with
    | ListExp l -> "(" + (String.concat " " (List.map toString l)) + ")"
    | SymbolExp s -> s